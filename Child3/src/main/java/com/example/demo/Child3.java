package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Child3 {

	public static void main(String[] args) {
		SpringApplication.run(Child3.class, args);
	}

}
